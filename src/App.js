import avatar from "./assets/images/avatar.jpg";
import appStyle from "./App.module.css";

function App() {
  return (
    <div>
      <div className={appStyle.devcampContainer}>
        <div>
          <img src={avatar} alt="Avatar" className={appStyle.devcampAvatar}/>
        </div>
        <div className={appStyle.devcampQuote}>
          <p>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
        </div>
        <div className={appStyle.devcampAuthor}>
          <b>
            Tammy Stevens 
          </b>
          &nbsp; * &nbsp;Front End Developer
        </div>
      </div>
    </div>

  );
}

export default App;
